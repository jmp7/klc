+++
title = "S7.1 Power symbols"
+++

Power symbols are special symbols in KiCad, used to designate global nets for power connections.

. Reference Designator must be set to `#PWR`
. Power symbols must contain exactly one pin, which is set to *invisible*
. Electrical pin type must be set to `Power Input`
. Pin name must match the symbol name
. Checkbox `Define as power symbol` must be checked

The example image below demonstrates the requirements as listed above:

{{< klcimg src="S7.1_a" title="Example power symbol" >}}
